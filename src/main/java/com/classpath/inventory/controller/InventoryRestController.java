package com.classpath.inventory.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoryRestController {

	
	private Integer qty = 1000;

	@GetMapping
	public Integer getQty() {
		return this.qty;
	}
	
	@PostMapping
	public Integer updateQty() {
		return --qty;
	}
}
