package com.classpath.inventory.event;

import java.time.LocalDate;

public class Order {
	private String customerName;
	
	private String customerEmail;
	
	private String password;
	
	private double orderPrice;
	
	private LocalDate orderDate;
	
	public Order() {}

	
	public Order(String customerName, String customerEmail, String password, double orderPrice, LocalDate orderDate) {
		super();
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.password = password;
		this.orderPrice = orderPrice;
		this.orderDate = orderDate;
	}


	public String getCustomerName() {
		return customerName;
	}


	public String getCustomerEmail() {
		return customerEmail;
	}


	public String getPassword() {
		return password;
	}


	public double getOrderPrice() {
		return orderPrice;
	}


	public LocalDate getOrderDate() {
		return orderDate;
	}


	@Override
	public String toString() {
		return "Order [customerName=" + customerName + ", customerEmail=" + customerEmail + ", password=" + password
				+ ", orderPrice=" + orderPrice + ", orderDate=" + orderDate + "]";
	}
	
	
	

}
