package com.classpath.inventory.event;

import java.time.LocalDateTime;


public class OrderEvent {
	private LocalDateTime eventTime;
	private OrderStatus orderStatus;
	private Order order;
	
	public OrderEvent() {
		super();
	}
	public LocalDateTime getEventTime() {
		return eventTime;
	}
	public void setEventTime(LocalDateTime eventTime) {
		this.eventTime = eventTime;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	@Override
	public String toString() {
		return "OrderEvent [eventTime=" + eventTime + ", orderStatus=" + orderStatus + ", order=" + order + "]";
	}
	
	
	
	

}
